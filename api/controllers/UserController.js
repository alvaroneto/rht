/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
    current: (req, res) => {
        if (req.token && req.token.user){
            return res.json(200, req.token.user);
        }
        return res.json(400, { error: "Invalid Token"});
    },
	users: (req, res) => {
        User.find({limit: 30})
        .populate(['position','permission'])
        .exec((err, records) => {
            return res.json(200, records)
        })
    }
};

