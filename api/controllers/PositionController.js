/**
 * PositionController
 *
 * @description :: Server-side logic for managing Positions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	positions: (req, res) => {
        Position.find()
        .exec((err, result) => {
            if (err) return res.json(500, err)
            async.each(result, (position, cb) => {
                User.count({
                    position: position.id
                })
                .exec((err, found) => {
                    _.map(result, (p) => {
                        if (p.id == position.id) position.subscribed = found; 
                    })
                    return cb(err, position);
                })
            }, 
            (err,position) => {
                if(err) return res.json(500, err);
                return res.json(200, result);
            })

        })
    }
};

