/**
 * FormController
 *
 * @description :: Server-side logic for managing Forms
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	create: (req, res) => {
        if(!req.token) return res.json(401,"Expired session")

        let data = req.body.schema, questions = [];
        data.schema = {
            title: data.title,
            description: data.description,
            properties: data.properties,
            type: 'object'
        }

        data["createdBy"] = req.token.user.id;

        JSON.stringify(data)
        if (!data.properties || data.properties && data.properties == {}) return res.json(400);
        Object.keys(data.properties).map((property, key) => {
            let type = _.contains(['textarea', 'text', 'string'], data.properties[property].type) ? 'essay' : 'objective'
            let question = { 
                uiproperty: property, 
                enunciation: data.properties[property].title,
                type: type,
                createdBy: req.token.user.id
            }
            questions.push(question);
        })

        Form.create(data)
        .then((result) => {
            return result;
        })
        .then((form) => {
            this.form = form;
            _.map(questions, (q) => {
                q.form = form.id;
            })
            return Question.create(questions)
        })
        .then((questions) => {
            this.form.questions = _.map(questions, (q) => q.id);
            return this.form.save();
        })
        .then(() => {
            return res.json(201, this.form);
        })
        .catch((err) => {
            return res.json(500, err);
        })
    }
};

