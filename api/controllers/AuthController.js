/**
 * AuthController
 *
 * @module		:: Controller
 * @description	:: Contains logic for handling auth requests.
 */

var passport = require('passport');

module.exports = {

	login: function(req,res){
        let user;
		passport.authenticate(
		    'local',
		    function(err, user, info)
		    {
		        if ((err) || (!user))
		        {
		            return res.json(400, "Bad Request");
		        }
				this.user = user;
		        // use passport to log in the user using a local method
		        req.logIn(
		            this.user,
		            function(err)
		            {
		                if (err)
		                {
		                    return res.json(400, "Bad Request");
		                }
		                return res.json(200, "OK");
		            }
		        );
		    }
		)(req, res);
	},

	logout: function(req,res){
		req.logout();
		return res.json(200, "OK");
	}
 
};
