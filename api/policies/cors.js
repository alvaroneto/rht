module.exports = function cors(req, res, next){
    // Enabling cors
    res.set('X-Powered-By', 'RHT');
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Content-Range, Content-Disposition, Content-Description, Accept");
    res.header("Access-Control-Allow-Methods", "POST, GET, DELETE, PUT, OPTIONS");
    res.header("Content-Type", "application/json;charset=UTF-8");
    next();
}