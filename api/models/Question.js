/**
 * Question.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    form: {
      model:'form'
    },
    enunciation: {
      type: 'string'
    },
    comments: {
      type: 'string'
    },
    uiproperty: {
      type: 'string'
    },
    alternatives: {
      collection: 'alternative',
      via: 'question'
    },
    type: {
      type: 'string',
      enum: ['objective', 'essay', 'multi-choice']
    }
  }
};

