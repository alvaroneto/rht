/**
 * Form.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    title: {
      type: 'string'
    },
    description: {
      type: 'string'
    },
    activeAt: {
      type: 'datetime'
    },
    createdBy: {
      model:'user'
    },
    reviewers: {
      collection: 'user',
      via: 'id'
    },
    answers: {
      collection: 'answer',
      via: 'form'
    },
    questions: {
      collection: 'question',
      via: 'form'
    },
    schema: {
      type: 'json'
    },
    uischema: {
      type: 'json'
    }
  }
};

