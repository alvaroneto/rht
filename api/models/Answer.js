/**
 * Answer.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */

module.exports = {
  attributes: {
    form: {
      model: 'form'
    },
    owner: {
      model: 'user'
    },
    question: {
      model: 'question'
    },
    response: {
      collection: 'answer',
      via: 'id'
    }
  }
};

