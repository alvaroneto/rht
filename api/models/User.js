/**
 * User.js
 *
 * @description :: TODO: You might write a short summary of how this model works and what it represents here.
 * @docs        :: http://sailsjs.org/documentation/concepts/models-and-orm/models
 */
var bcrypt = require('bcrypt');

module.exports = {
  attributes: {
    firstName: {
      type: 'string'
    },
    lastName: {
      type: 'string'
    },
    email: {
      type: 'email',
      unique: true,
      required: true
    },
    phoneNumber: {
      type: 'string'
    },
    gender: {
      type: 'string',
      enum: ['male', 'female'],
      defaultsTo: 'male'
    },
    profilePhoto: {
      type: 'string'
    },
    fullAddress: {
      type: 'string'
    },
    position:{
      model: 'position',
      dominant: true
    },
    permission: {
      model: 'permission',
      dominant: true
    },
    hashedPassword: {
        type: 'string',
    },
    // Override toJSON method to remove password from API
    toJSON: function() {
      var obj = this.toObject();
      delete obj.password;
      return obj;
    }
  },
  beforeCreate: function(values, next){
    bcrypt.genSalt(10, function(err, salt) {
      bcrypt.hash(values.password, salt, function(err, hash) {
        if(err) return next(err);
        values.hashedPassword = hash;
        delete values.password;
        next();
      });
    });
  }
};

